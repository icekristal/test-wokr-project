<?php



use fnc\Security;
use fnc\Validator;


include_once S_ROOT. '/models/BlogModel.php';

class IndexController extends SetSmart implements ControllerInf
{

    public function IndexAction()
    {


        $last_blog_article = BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,1,4);

        $category_blog = BlogModel::getAllCategoryBlog();

        require_once(S_ROOT . '/views/blog/header.php');
        require_once(S_ROOT . '/views/blog/index.php');
        require_once(S_ROOT . '/views/blog/footer.php');

        return true;
    }



}

