<?php


use fnc\Debug;
use fnc\ImageUpload;
use fnc\Logger;
use fnc\MessageVoice;
use fnc\Security;
use fnc\Translate;
use fnc\Validator;



class AdminController extends SetSmart implements ControllerInf
{

    public function __construct()
    {
        parent::__construct();


       // UsersTopModel::checkRightUser(1);



        $usertop=$this->UserInfoAuth()->getUserSession();


    }


            //Страница главная админки
    public function IndexAction()
    {

        $count_all_article=BlogModel::getCountAllArticle();
        $count_all_top_users=UsersTopModel::getCountAllTopUsers();


        require_once(S_ROOT . '/views/admin/header.php');
        require_once(S_ROOT . '/views/admin/index.php');
        require_once(S_ROOT . '/views/admin/footer.php');
        return true;
    }

    //Страница нет прав
    public function NorightAction()
    {
        $this->LoadSmarty("header");
        $this->LoadSmarty("notright");
        $this->LoadSmarty("footer");
        return true;
    }

    public function LogoutAction(){
        if(isset($_SESSION['usertop'])){
            unset($_SESSION['usertop']);
        }
        UsersTopModel::CheckLoginUser();
    }


            //Страница блога

    /**
     * @return bool
     */
    public function BlogAction()
    {

        //Тестовое поехали
        $all_article = BlogModel::getAllArticleAdmin();
        $rsCategory=BlogModel::getAllCategoryBlog();
        $categorpub=$all_article;

        require_once(S_ROOT . '/views/admin/header.php');
        require_once(S_ROOT . '/views/admin/blog.php');
        require_once(S_ROOT . '/views/admin/footer.php');
        return true;
    }
        /*
         * Добавляем категорию в пубилкации
         *
         */
         public function AddnewcatpublicAction(){


            $name_category = $_POST['new_category'];
            $rezData = array();
            $check = true;


            if($check){
                BlogModel::makeNewCategoryForBlog($name_category);
                $rezData['message']= $this->MessageSend(2,"Добавлено");
                $rezData['success']=1;
              //  $this->Logger(2,$this->UserInfoAuth()->getUserHash(),"Создал категорию {$name_category}");
            }
            echo json_encode($rezData,error_reporting());

        }

        /*
         * Добавляем новую публикацию
         */
         public function AddnewpublicationAction(){
        if(!Validator::isJson()) redirect("/error/");


        $category_id = intval($_POST['category']);
        $title = Security::FormChar($_POST['title']);
        $title_url = Translate::str2url($title);
        $descriprion = Security::FormChar($_POST['description']);
        $text = $_POST['textarticle'];
        $day_t = $_POST['day'];
        $time_t = $_POST['time'];
        $datatimepublic=$day_t.' '.$time_t;
        $file = $_FILES['image_pub'];
        $str_file = Translate::RandomString(3);
        $check = true;
        $rezData = array();
        $rest = substr($title_url, 0, 4);
        $name_file = $category_id.'-'.$rest.'-'.$str_file;
        $file_download = 'image/download/blog_main/';


           if($check){

      $result_send =  ImageUpload::uploadimage($file,$file_download,$name_file,1,805,385);
        if(!$result_send['success']){
            $check = false;
            $rezData['success']=0;
            $rezData['message']= $this->MessageSend(1,$result_send['message']);
            $this->Logger(1,$this->UserInfoAuth()->getUserHash(),"большое изображение в {$title}");
        }
}
        if($check){

            $rs_create = BlogModel::AddPublication($category_id,$title,$title_url,$descriprion,$text,$datatimepublic,$result_send['file_name']);
            if($rs_create){
                $rezData['success'] = 1;
                $rezData['message'] = $this->MessageSend(2,'Публикация успешно создана - '.$title);
             //   writelogfile('admin',$_SESSION['usertop']['login']," Создание публикации $title",2);
               $this->Logger(2,$this->UserInfoAuth()->getUserHash(),'Публикация успешно создана - '.$title);
            }else{
                $rezData['success'] = 0;
                $rezData['message'] = $this->MessageSend(1,' Создание публикации не удачно - '.$title_url.$str_file);
                $this->Logger(1,$this->UserInfoAuth()->getUserHash(),"Создание публикации не удачно{$title}");

            }
        }

        echo json_encode($rezData,error_reporting());
    }

        /*
         * Удаление/скрытие категории
         */
        public function DeletecategorypublicationAction(){
            if(!Validator::isJson()) redirect("/error/");
            $id = intval($_POST['id']);
            $chnge = intval($_POST['visible']);
            $rezData = array();
            if($chnge == 1){
                $chnge = 0;
            }else{
                $chnge = 1;
            }
            $rs_vis = BlogModel::DeleteCategoryPublication($id,$chnge);

            if($rs_vis){
                $rezData['success'] = 1;
                $rezData['message'] = $this->MessageSend(2,'Категория удалена ');
                $this->Logger(2,$this->UserInfoAuth()->getUserHash(),"Категория скрыта id - $id");
            }else{
                $rezData['success'] = 0;
                $rezData['message'] = $this->MessageSend(1,' Категория публикации не удачно - ');
                $this->Logger(1,$this->UserInfoAuth()->getUserHash(),"Категория публикации не удачно - $id");

            }
            echo json_encode($rezData);
        }
         /*
     * Выводим публикацию
     */
         public function ViewonepublicAction(){
            if(!Validator::isJson()) redirect("/error/");
            $rezData = array();
            $id = intval($_POST['id']);
            $html = "<b style='color: red;'>Что-то пошло не так</b>";
            $rs = BlogModel::getArticleForId($id);
            $category_select = "";
            if($rs){
                $all_cat_public = BlogModel::getAllCategoryBlog();

                $category_select = "<option value='$rs[category]' selected>$rs[name]</option>";
                foreach ($all_cat_public as $pubcat){
                    $category_select .="<option value='$pubcat[id]'>$pubcat[name]</option>";
                }

                $datatime_pub = explode(' ',$rs['time_public']);
                $data_pub = $datatime_pub[0];
                $time_pub = $datatime_pub[1];
                $html = "
            <form id=\"editblog\" enctype=\"multipart/form-data\" action=\"javascript:void(null);\" onsubmit=\"EditBlog();\">
							 <div class=\"col-md-5 col-xs-12\">
								 <select name=\"category\" id=\"category\" required class=\"form-control\">									
                                        $category_select
								 </select>
								 <label for=\"title\">Яркий заголовок, максимум 70 символов, минимум 40. Из знаков препинания можно только ,!?</label>
								 <input value='$rs[title]' type=\"text\" id=\"title\" name=\"title\" required placeholder=\"Заголовок\" class=\"form-control\">
								 <label for=\"description\">Краткое содержание статьи, минимум 150 символов, максимум 200. Это для красивого вывода! Пишите чтоб хотелось прочитать!
								 </label>
								 <textarea name=\"description\" id=\"description\" class=\"form-control\" required placeholder=\"Краткое содержание статьи\">$rs[description]</textarea>
                            <label for=\"image_pub\">Начальное изображение</label>
                            <input type=\"file\" onchange=\"previewFile('editblog','seefastimgpube')\" id=\"image_pub\" name=\"image_pub\" class=\"form-control\">

								 <label for=\"day\">Дата публикации</label>
								 <input value='$data_pub' type=\"date\" id=\"day\" name=\"day\" class=\"form-control\" required>
								 <input value='$time_pub' type=\"time\" name=\"time\" class=\"form-control\" required>
							<img src=\"/image/download/blog_main/$rs[file]\" id=\"seefastimgpube\" width=\"250\">

							 </div>

							 <div class=\"col-md-7 col-xs-12\">
								 <label for=\"text\">Полное описание публикации
								 </label>
								 <textarea  class=\"form-control\"  name=\"textarticle\" id=\"textarq\" rows=\"15\" required>$rs[text_article]</textarea>
                            
							 </div>

							 <div class=\"col-md-12 col-xs-12 text-center\" style=\"margin-top: 15px;\">
							 <input type='hidden' value='$rs[id]' name='id'>
							 <input type='hidden' value='$rs[file]' name='old_image'>
						
								 <input type=\"submit\" class=\"btn btn-warning form-control\" value=\"Сохранить\" id=\"butsaveblog\" onclick=\"CKupdate();\">
							 </div>

						 </form>

						 <script type=\"text/javascript\">
							 CKEDITOR.replace( 'textarq' );
						 </script> ";
            }
            $rezData['success']=1;
            $rezData['message']=$html;
            echo json_encode($rezData);
        }

        /*
         * Редактируем блог
         */
        public function EditpublicationAction(){
            if(!Validator::isJson()) redirect("/error/");
            $category_id = intval($_POST['category']);
            $id = intval($_POST['id']);
            $title = Security::FormChar($_POST['title']);
            $title_url = Translate::str2url($title);
            $descriprion = Security::FormChar($_POST['description']);
            $text = $_POST['textarticle'];
            $day_t = $_POST['day'];
            $time_t = $_POST['time'];
            $datatimepublic=$day_t.' '.$time_t;
            $file = $_FILES['image_pub'];
            $filecc = $_FILES['image_pub']['name'];
            $name_image = $_POST['old_image'];
            $str_file = Translate::RandomString(3);
            if(!empty($filecc)) {
                $dir = '../www/image/download/blog_main/'.$name_image;
                if(file_exists($dir)){
                    unlink($dir);
                }

                $rest = substr($title_url, 0, 4);
                $name_file = $category_id.'-'.$rest.'-'.$str_file;
                $file_download = 'image/download/blog_main/';
                $result_send = ImageUpload::uploadimage($file,$file_download,$name_file,1,805,385);

                if($result_send['success']){
                    $name_image = $result_send['file_name'];

                }else{
                  //  writelogfile('admin',$_SESSION['usertop']['login'],"Загрузка большого изображение для публикации при редактировании id - $id",1);

                    $rezData['success'] = 0;
                    $rezData['message'] = $this->MessageSend(1,'Слишком большое изображение - максимум 2МБ');
                    echo json_encode($rezData);
                    exit();
                }
            }

            $rezData = array();
            $rs_edit = BlogModel::UpdatePublication($id,$category_id,$title,$title_url,$descriprion,$text,$datatimepublic,$name_image);
            if($rs_edit){
                $rezData['success'] = 1;
                $rezData['message'] = $this->MessageSend(2,'Публикация успешно отредактирована - '.$title);
              //  writelogfile('admin',$_SESSION['usertop']['login'],"Отредактировал публикацию - $title",2);
                $this->Logger(2,$this->UserInfoAuth()->getUserHash(),'Публикация успешно отредактирована - '.$title);

            }else{
                $rezData['success'] = 0;
                $rezData['message'] = $this->MessageSend(1,' Редактирование публикации не удачно код 422 - '.$title);
                $this->Logger(1,$this->UserInfoAuth()->getUserHash(),'Публикация НЕ отредактирована - '.$title);

            }
            echo json_encode($rezData,error_reporting());
        }

        /*
         * Удаление/скрытие публикации
         */
        public function DeletepublicationAction(){
            if(!Validator::isJson()) redirect("/error/");
            $id = intval($_POST['id']);
            $chnge = intval($_POST['visible']);
            $rezData = array();
            if($chnge == 1){
                $chnge = 0;
            }else{
                $chnge = 1;
            }
            $rs_vis = BlogModel::DeletePublication($id,$chnge);

            if($rs_vis){
                $rezData['success'] = 1;
                $rezData['message'] = $this->MessageSend(2,'Публикация успешно скрыта ');
                $this->Logger(2,$this->UserInfoAuth()->getUserHash(),"Публикация скрыта id - $id");
            }else{
                $rezData['success'] = 0;
                $rezData['message'] = $this->MessageSend(1,' Скрыта публикации не удачно - ');
                $this->Logger(1,$this->UserInfoAuth()->getUserHash(),"Скрыта публикации не удачно - $id");

            }
            echo json_encode($rezData);
        }




     

}