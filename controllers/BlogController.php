<?php




use fnc\Security;
use fnc\Validator;


class BlogController extends SetSmart implements ControllerInf
{

            //Страницы

    public function IndexAction($page=1)
    {

        $all_article = BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,$page,null);
        $count_article = BlogModel::getCountAllArticle();


        $all_category_blog=BlogModel::getAllCategoryBlog();
        $last_blog_article=BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,1,2);






        require_once(S_ROOT . '/views/blog/header.php');
        require_once(S_ROOT . '/views/blog/blog.php');
        require_once(S_ROOT . '/views/blog/footer.php');
        return true;
    }

    public function CategoryAction($category,$page=1){
            $check_id = BlogModel::checkExistCategory($category);
        if(!$check_id) redirect("/error/");

         $all_article_category = BlogModel::getAllArticleForCategory(BlogModel::SHOW_DEFAULT_ARTICLE,$page,$check_id,null);
        $count_article_category = BlogModel::getCountAllArticle($check_id);

        $info_category = BlogModel::getOneCategoryBlog($check_id);
        $last_blog_article = BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,1,2);

        $all_category_blog = BlogModel::getAllCategoryBlog();




        require_once(S_ROOT . '/views/blog/header.php');
        require_once(S_ROOT . '/views/blog/blog.php');
        require_once(S_ROOT . '/views/blog/footer.php');


        return true;
    }

    public function ArticleAction($category,$title_article){
        $check_id = BlogModel::checkExistCategory($category);
        if(!$check_id) redirect("/error/");

          $check_article_id = BlogModel::checkExistrticle($title_article);
          if(!$check_article_id) redirect("/error/");

    $info_article = BlogModel::getArticleForId($check_article_id);
          if(!$info_article) redirect("/error/");

          $right_bar_article=BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,1,3);

        $last_blog_article=BlogModel::getAllArticle(BlogModel::SHOW_DEFAULT_ARTICLE,1,2);
        $all_category_blog = BlogModel::getAllCategoryBlog();

          BlogModel::setCountReadArticle($check_article_id);




        require_once(S_ROOT . '/views/blog/header.php');
        require_once(S_ROOT . '/views/blog/article.php');
        require_once(S_ROOT . '/views/blog/footer.php');
        return true;
    }

}