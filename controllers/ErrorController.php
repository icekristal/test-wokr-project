<?php

use \components\SetSmart as BaseSmart;
use components\ControllerInf;
use models\BlogModel;


class ErrorController extends BaseSmart implements ControllerInf
{

    public function IndexAction()
    {
        $this->assign("TitleTwo","Ошибка страница не найдена");
        header('HTTP/1.0 404 Not Found');

        $this->LoadSmarty("header");
        $this->LoadSmarty("error");
        $this->LoadSmarty("footer");
        return true;
    }
}