<div id="page-wrapper" class="sign-in-wrapper">
    {if !isset($arrayTopUser)}
    <div class="graphs">
        <div class="sign-up">

            <form method="POST" id="registrationtopform" enctype="multipart/form-data" action="javascript:void(null);" onsubmit="registerNewUserTop();">

            <h3>Регистрация</h3>
            <p class="creating">
                После регистрации дождитесь выдачи прав доступа
            </p>

            <h5>Персональная информация</h5>
                <div class="sign-u">
                    <div class="sign-up1">
                        <h4>Логин* :</h4>
                    </div>
                    <div class="sign-up2">

                        <input type="text" placeholder="" required name="login"/>

                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="sign-u">
                    <div class="sign-up1">
                        <h4>Email* :</h4>
                    </div>
                    <div class="sign-up2">

                        <input type="email" placeholder=" " required name="email"/>

                    </div>
                    <div class="clearfix"> </div>
                </div>
            <div class="sign-u">
                <div class="sign-up1">
                    <h4>Имя* :</h4>
                </div>
                <div class="sign-up2">

                        <input type="text" placeholder=" " required name="name"/>

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="sign-u">
                <div class="sign-up1">
                    <h4>Фамилия* :</h4>
                </div>
                <div class="sign-up2">

                        <input type="text" placeholder=" " required name="s_name"/>

                </div>
                <div class="clearfix"> </div>
            </div>

            <h6>Придумайте пароль</h6>
            <div class="sign-u">
                <div class="sign-up1">
                    <h4>Пароль* :</h4>
                </div>
                <div class="sign-up2">

                        <input type="password" placeholder=" " required name="password"/>

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="sign-u">
                <div class="sign-up1">
                    <h4>Повтор пароля* :</h4>
                </div>
                <div class="sign-up2">

                        <input type="password" placeholder=" " required name="passwordq"/>

                </div>
                <div class="clearfix"> </div>
                <p id="MessageRegistration"></p>
            </div>
            <div class="sub_home">

                <div class="sub_home_left">

                        <input type="submit" value="Отправить">

                </div>
                <div class="sub_home_right">
                    <p>Уже зарегистрированы? <a href="{$hrefadmin}/auth/">Вход</a></p>
                </div>
                <div class="clearfix"> </div>
            </div>
    </form>
        </div>

    </div>
        {else}
        <div class="sign-in-form-top">
            <p><span>Вход в </span> <a href="{$hrefadmin}">Админ Панель</a></p>
        </div>
    {/if}
</div>
