<!-- left side start-->
<div class="left-side sticky-left-side">

    <!--logo and iconic logo start-->
    <div class="logo">
        <h1><a href="/admin"><span>TestBlog</span></a></h1>
    </div>
    <div class="logo-icon text-center">
        <a href="/admin"><i class="lnr lnr-home"></i> </a>
    </div>

    <!--logo and iconic logo end-->
    <div class="left-side-inner">

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
            <li><a href="/admin/blog/"><i class="lnr lnr-pencil"></i> <span>Блог</span></a></li>
        </ul>
        <!--sidebar nav end-->
    </div>
</div>
<!-- left side end-->