<div class="main-content">
    <!-- header-starts -->
    <?php include_once "include-menu.php"; ?>
    <!-- //header-ends -->
    <div id="page-wrapper">
        <div class="graphs">
            <h4 class="blank1">Раздел блога</h4>
            <p class="desc_razdel">
                В данном разделе происходит написание публикаций для сайта
                <br>
            </p>
            <div class="bs-example5" data-example-id="default-media">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createpublic">Создать публикацию</button>
                        <div class="refseshtoplist">
                            <div class="refseshbottomlist">
                                <h3>Список всех публикаций :</h3>

                                <div class="table-responsive">
                                    <div class="input-group"> <span class="input-group-addon">Фильтр</span>
                                        <input id="filter" type="text" class="form-control" placeholder="Категория... и.т.д">
                                    </div>
                                    <table class="table table-bordered" style="font-size: 13px;">
                                        <tr>

                                            <th>Категория</th>
                                            <th>Заголовок</th>
                                            <th><i class="fa fa-eye" aria-hidden="true"></i></th>
                                            <th>Даты</th>
                                            <th>***</th>
                                        </tr>
                            <?php if (isset($categorpub[0])){ ?>
                                        <tbody class='searchableblog'>
                                        <?php foreach ($categorpub as $pub){
                                            if ($pub['visible'] == 1) {
                                                $color = '#CEF6CE';
                                            } else{
                                                $color = '#F5BCA9';
                                        }

                                        ?>
                                        <tr style="background-color: <?= $color ?>">

                                            <td><?= $pub['name'] ?></td>
                                            <td><?= $pub['title'] ?></td>
                                            <td><?= $pub['count_read'] ?></td>
                                            <td><b><?= $pub['time_public'] ?></b> / <?= $pub['data_dob'] ?></td>
                                            <td>
                                                <i onclick="SeePublicInModal('<?= $pub['id'] ?>')" class="fa fa-eye"
                                                   aria-hidden="true" style="cursor:pointer;" data-toggle="modal"
                                                   data-target="#editpublic"></i>
                                                <i onclick="if(confirm('Скрыть публикацию?')) { DeletePublic('<?= $pub['id'] ?>','<?= $pub['visible'] ?>'); } return false"
                                                   class="fa fa-times" aria-hidden="true" style="cursor:pointer;"></i>

                                            </td>
                                        </tr>

                                        <?php }
                                        ?>
                                        </tbody>
                                        <?php }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12">
                        <h3>Добавление категории блога</h3>
                        <form method="POST" id="addnewcategory" enctype="multipart/form-data" action="javascript:void(null);" onsubmit="addnewcategory();">
                            <input type="text" class="form-control" name="new_category" required placeholder="Новая категория">
                            <select name="new_category_public" id="new_category_public" class="form-control" title="Добавление новой категории">
                                <option value="0" selected>Основная категория</option>
                            </select>
                            <!--  <input type="file" onchange="previewFile('addnewcategory','seefastimgpubcat')" name="image_category_public" id="image_category_public" required class="form-control"> -->
                            <input type="submit" value="Добавать" id="publication_add_button" class="form-control">
                        </form>
                        <img src="" id="seefastimgpubcat" width="250">
                        <div id="placemessage"></div>


                        <div class="block_cat_blog">
                            <?php if (isset($rsCategory[0])) {
                                foreach ($rsCategory as $cat) { ?>

                                    <div id="catblog_<?=$cat['id']?>" >
                                        <?=$cat['name']?> - <i onclick = "if(confirm('Удалить категорию?')) { DeleteCategoryPublic('<?=$cat['id']?>','<?=$cat['visible']?>'); } return false" class="fa fa-times" aria - hidden = "true" style = "cursor:pointer;" ></i >

                            </div>

                          <?php
                            }
                            }
                            ?>
                        </div>


                    </div>

                </div>


            </div>
        </div>
        <!--body wrapper start-->
    </div>
    <!--body wrapper end-->
</div>

<div class="modal fade" id="createpublic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Создание публикации - <span id="messageblog"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row" id="oknocreateblog">

                    <form id="createblog" enctype="multipart/form-data" action="javascript:void(null);" onsubmit="CreateBlog();">
                        <div class="col-md-5 col-xs-12">
                            <select title="" name="category" id="category" required class="form-control">
                                <option  value="" selected disabled>Выберите категорию...</option>
                                <?php foreach ($rsCategory as $categ) { ?>
                                    <option value="{$categ['id']}"><?=$categ['name']?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <label for="title">Заголовок</label>
                            <input type="text" id="title" name="title"  required placeholder="Заголовок" class="form-control" title="Друг надо следовать четко интрукции, если что точки ставить нельзя! Только запятая">
                            <label for="description">Краткое содержание статьи
                            </label>
                            <textarea name="description" id="description" class="form-control" required placeholder="Краткое содержание статьи"></textarea>

                            <label for="day">Дата публикации</label>
                            <input type="date" id="day" name="day" class="form-control" required>
                            <input type="time" name="time" class="form-control" required title="">

                            <label for="image_pub">Изображение</label>
                            <input type="file" onchange="previewFile('createblog','seefastimgpub')" id="image_pub" name="image_pub" required class="form-control">

                            <img src="" id="seefastimgpub" width="250">
                            <br>
                            <!--   <label for="send_email_public">Сделать рассылку по почте?</label>
                               <input id="send_email_public" type="checkbox" name="emailsend" checked value="yes"> -->
                        </div>

                        <div class="col-md-7 col-xs-12">
                            <span id="textsimvosn" style="color: red;"></span>
                            <label for="text">Полное описание публикации
                            </label>


                            <textarea class="form-control" title="Текст статьи" name="textarticle" id="textar" rows="15" required></textarea>
                            <script>
                                CKEDITOR.replace('textar');
                            </script>
                        </div>

                        <div class="col-md-12 col-xs-12 text-center" style="margin-top: 15px;">
                            <input type="submit" class="btn btn-warning form-control" value="Создать" id="button_create_blog" onclick="CKupdate();">
                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>


</div>
<div class="modal fade" id="editpublic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Редактирование публикации </h4>
                <div id="message_edit_blog"></div>
            </div>
            <div class="modal-body">
                <div class="row" id="oknoeditblog">

                </div>

            </div>
        </div>
    </div>


</div>
<script>

    $(document).ready(function () {

        (function ($) {

            $('#filter').keyup(function () {

                var rex = new RegExp($(this).val(), 'i');
                $('.searchableblog tr').hide();
                $('.searchableblog tr').filter(function () {
                    return rex.test($(this).text());
                }).show();

            })

        }(jQuery));

    });
</script>
