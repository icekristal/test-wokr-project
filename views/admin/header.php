<!DOCTYPE HTML>
<html>
<head>
    <title>Админ Панель</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <!-- Bootstrap Core CSS -->
    <link href="/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= TemplateAdminWebPath ?>css/style.css" rel='stylesheet' type='text/css' />
    <!-- jQuery -->
    <!-- lined-icons -->
    <link rel="stylesheet" href="<?= TemplateAdminWebPath ?>css/icon-font.min.css" type='text/css' />
    <!-- //lined-icons -->
    <!-- chart -->
    <script src="<?= TemplateAdminWebPath ?>js/Chart.js"></script>
    <!-- //chart -->
    <!--animate-->
    <link href="<?= TemplateAdminWebPath ?>css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="<?= TemplateAdminWebPath ?>js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!--//end-animate-->
    <!----webfonts--->
    <link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


    <!---//webfonts--->
    <!-- Meters graphs -->
    <script src="/assets/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?= TemplateAdminWebPath ?>js/ckeditor/ckeditor.js"></script>

</head>

<body class="sticky-header left-side-collapsed">
    <section>

       <?php if (isset($usertop[0])){ ?>
           <?php
           include_once "include-menu.php";
           ?>
        <?php } ?>
