
<!-- ===================================
    FOOTER
======================================== -->
<footer class="def-section footer">
    <div class="container">
        <div class="row">

            <!-- === FOOTER COLUMN === -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-1">
                <div class="logo with-border-bottom">
                    <div class="logo-image">
                        <img src="/image/native/logo.png" alt="" />
                    </div>
                    <div class="logo-text">
                        <span class="color-primary">Тест блог</span>
                    </div>
                </div>
                <div class="footer-1-text">

                </div>
                <div class="footer-1-button">

                </div>
            </div>

            <!-- === FOOTER COLUMN === -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-2">
                <h4 class="with-square with-border-bottom">Меню</h4>
                <div class="footer-2-links">
                    <div class="footer-2-links-1">
                        <ul>
                            <li><a href="/">Главная</a></li>
                            <li><a href="/blog">Блог</a></li>
                        </ul>
                    </div>
                    <div class="footer-2-links-2">

                    </div>
                </div>
            </div>

            <!-- === FOOTER COLUMN === -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-3">
                <h4 class="with-square with-border-bottom">Связь с нами</h4>
                <div class="footer-3-phone footer-3-item">
                    <span class="footer-3-icon"><i class="fa fa-phone"></i></span>
                    Телефон:  +7 (777) 999-55-00
                </div>
                <div class="footer-3-mail footer-3-item">
                    <span class="footer-3-icon"><i class="fa fa-envelope"></i></span>
                    E-mail:  info@test.web
                </div>
            </div>

            <!-- === FOOTER COLUMN === -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 footer-4">
                <h4 class="with-square with-border-bottom">Красивые фотки</h4>
                <div class="footer-4-widget">

                </div>
            </div>

        </div>
    </div>
</footer>
<!-- ===================================
    END FOOTER
======================================== -->


<!-- ===================================
    BOTTOM SECTION
======================================== -->
<div class="bottom">
    <div class="container">
        <div class="row">

            <!-- === BOTTOM LEFT === -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-1">
                COPYRIGHT 2017 | <span class="color-primary">Savatneev Anton</span>
            </div>

            <!-- === BOTTOM CENTER === -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-2">
                <a href="#"><div class="my-btn my-btn-grey">
                        <div class="my-btn-bg-top"></div>
                        <div class="my-btn-bg-bottom"></div>
                        <div class="my-btn-text">
                            <i class="fa fa-vk"></i>
                        </div>
                    </div></a>

            </div>

            <!-- === BOTTOM RIGHT === -->
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bottom-3">

            </div>

        </div>
    </div>
</div>
<!-- ===================================
    END BOTTOM SECTION
======================================== -->


<!-- =========================
   SLIDE MENU
============================== -->
<aside id="slide-menu" class="slide-menu">

    <!-- === CLOSE MENU BUTON === -->
    <div class="close-menu" id="close-menu">
        <i class="fa fa-close"></i>
    </div>

    <!-- === SLIDE MENU === -->
    <ul id="left-menu" class="left-menu">

        <!-- === SLIDE MENU ITEM === -->
        <li>
            <a href="/">Главная <i class="fa fa-plus arrow"></i></a>
        </li>
        <!-- === SLIDE MENU ITEM === -->
        <li>
            <a href="#">Блог <i class="fa fa-plus arrow"></i></a>


        </li>


    </ul>

</aside>
<!-- =========================
   END SLIDE MENU
============================== -->


<!-- =========================
   BLACK OVERLAY
============================== -->
<div class="black-overlay" id="black-overlay"></div>
<!-- =========================
   END BLACK OVERLAY
============================== -->

<!-- =========================
     SCRIPTS
============================== -->

<!-- JQUERY -->
<script src="/assets/jquery/dist/jquery.min.js"></script>

<!-- BOOTSTRAP -->
<script src="/assets/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- SMOOTH SCROLLING  -->
<script src="<?= TemplateWebPath ?>js/smoothscroll.min.js"></script>

<!-- STELLAR.JS FOR PARALLAX -->
<script src="<?= TemplateWebPath ?>js/jquery.stellar.min.js"></script>

<!-- SLIDER PRO  -->
<script src="/assets/slider-pro/dist/js/jquery.sliderPro.min.js"></script>

<!-- SCROLLSPY -->
<script src="<?= TemplateWebPath ?>js/scrollspy.min.js"></script>

<!-- WOW PLAGIN -->
<script src="<?= TemplateWebPath ?>js/wow.min.js"></script>

<!-- CAROUSEL -->
<script src="/assets/owl-carousel/dist/owl.carousel.min.js"></script>

<!-- VERTICAL ACCORDEON MENU -->
<script src="<?= TemplateWebPath ?>js/metisMenu.min.js"></script>

<!-- CUSTOM SCRIPT -->
<script src="<?= TemplateWebPath ?>js/theme.min.js"></script>


</body>
</html>