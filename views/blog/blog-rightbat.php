<div class="blog-right-bar">

    <div class="blog-right-bar-item">
        <h3>Случайные</h3>
        <div class="recent-posts">
            <?php if (isset($last_blog_article[0])){ ?>
<?php foreach ($last_blog_article as $art_right){ ?>
                    <div class="recent-post">
                        <div class="recent-post-image">
                            <img src="/image/download/blog_main/<?=$art_right['file']?>" alt="<?= $art_right['title']?>" />
                        </div>
                        <div class="recent-post-title">
                            <a href="<?= $art_right['href_article']?>"><?=$art_right['title']?></a>
                        </div>
                        <div class="recent-post-date">
                            <span class="date-icon"><i class="fa fa-calendar"></i></span>
                            <a href="<?= $art_right['href_article']?>"><?=$art_right['date_blog_rightbar']?></a>
                        </div>
                    </div>
               <?php
            }}
            ?>
        </div>
    </div>

    <div class="blog-right-bar-item">
        <h3>Категории</h3>
        <ul class="blog-categories">
<?php if (isset($all_category_blog[0])) { ?>
    <?php foreach($all_category_blog as $cat_blog){ ?>
    <li class="">
        <a href="/blog/<?= $cat_blog['keyc']?>"><?=$cat_blog['name']?></a>
    </li>
    <?php

} }
    ?>

        </ul>
    </div>




</div>