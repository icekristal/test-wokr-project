

<!-- =========================
    SERVICES
============================== -->
<section class="def-section home-services">
    <div class="container">
        <div class="row">

               <?php if (isset($category_blog[0])){ ?>
            <!-- === SERVICE ITEM === -->
                   <?php foreach($category_blog as $cat_bl){ ?>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">


                <div class="service">
                    <div class="service-icon">
                        <i class="flaticon-transport358"></i>
                    </div>
                    <h3> <?= $cat_bl['name'] ?></h3>
                    <div class="service-text">

                    </div>
                    <div class="service-button">
                        <a href="/blog/<?= $cat_bl['keyc'] ?>">
                            <div class="my-btn my-btn-default">
                                <div class="my-btn-bg-top"></div>
                                <div class="my-btn-bg-bottom"></div>
                                <div class="my-btn-text">
                                    Смотреть
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
                   <?php } ?>
                   <?php } ?>

        </div>
    </div>
</section>
<!-- =========================
   END SERVICES
============================== -->




<!-- ===================================
    BLOG SECTION
======================================== -->
<section class="def-section home-blog">
    <div class="container">
        <div class="row">

            <!-- === TITLE GROUP === -->
            <div class="title-group">
                <h2>Наши статьи</h2>
                <div class="subtitle with-square">Узнай все самое интересное</div>
            </div>

              <?php
              if (isset($last_blog_article[0])){
                 ?>
            <?php foreach ($last_blog_article as $article){
               ?>
            <!-- === BLOG ITEM === -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="home-blog-item">
                    <div class="home-blog-item-date">
                <?= $article['date_blog'] ?>
                    </div>
                    <div class="home-blog-item-image">
                        <img src="/image/download/blog_main/<?=  $article['file'] ?>" alt="<?= $article['title'] ?>" />
                    </div>
                    <div class="home-blog-item-desc">
                        <div class="home-blog-item-desc-title">
                            <a href="<?= $article['href_article']  ?>"><?= $article['title'] ?></a>
                        </div>
                        <div class="home-blog-item-desc-info">
                            <a href="#">Admin</a>
                        </div>
                        <div class="home-blog-item-desc-text">
                            <p><?= $article['description'] ?>...</p>
                        </div>
                    </div>
                </div>
            </div>

           <?php }
              } ?>

            <!-- === BLOG READ MORE BUTTON === -->
            <div class="home-blog-more col-xs-12 col-md-12">
                <a href="/blog"><div class="my-btn my-btn-primary">
                        <div class="my-btn-bg-top"></div>
                        <div class="my-btn-bg-bottom"></div>
                        <div class="my-btn-text">
                            Читать еще
                        </div>
                    </div></a>
            </div>

        </div>
    </div>
</section>
<!-- ===================================
    END BLOG SECTION
======================================== -->

<!-- ===================================
    CLIENTS SECTION
======================================== -->

<!-- ===================================
    END CLIENTS SECTION
======================================== -->

<!-- ===================================
    SUBSCRIBE SECTION
======================================== -->

<!-- ===================================
    END SUBSCRIBE SECTION
======================================== -->
