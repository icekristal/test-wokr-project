
<!-- ===================================
    PAGE HEADER
======================================== -->
<div class="page-header" data-stellar-background-ratio="0.4">
    <div class="page-header-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- === PAGE HEADER TITLE === -->
            <div class="page-header-title">
                <h2><?= $info_article['title'] ?></h2>
            </div>

            <!-- === PAGE HEADER BREADCRUMB === -->
            <div class="page-header-breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/blog">Блог</a></li>
                    <li class="active">Статья</li>
                </ol>
            </div>


        </div>
    </div>
</div>
<!-- ===================================
    END PAGE HEADER
======================================== -->

<!-- =========================
    BLOG ITEMS
============================== -->
<div class="def-section single-blog-section">
    <div class="container">
        <div class="row">

            <!-- === BLOG ITEMS === -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">


                <!-- === BLOG ITEM === -->
                <div class="single-post">
                    <div class="single-post-date">
<?= $info_article['date_blog']?>
                    </div>
                    <div class="single-post-image">
                        <img src="/image/download/blog_main/<?=$info_article['file']?>" alt="<?=$info_article['title']?>" />
                    </div>
                    <div class="single-post-info">
                        <span class="author-icon"><i class="fa fa-user"></i></span>
                        <a href="#">Admin</a>
                        <span class="date-icon"><i class="fa fa-calendar"></i></span>
                        <a href="<?=$info_article['href_article']?>"><?=$info_article['date_blog_rightbar']?></a>
                    </div>
                    <div class="single-post-title">
                        <h3><a href="<?=$info_article['href_article']?>"><?=$info_article['title']?></a></h3>
                    </div>
                    <div class="single-post-text">
<?=$info_article['text_article']?>

                    </div>

                    <div class="single-post-bb">
                        <div class="single-post-bb-arrow"><i class="flaticon-direction248"></i></div>
                        <div class="single-post-bb-line"></div>
                    </div>

                </div>

            </div>

            <!-- === BLOG RIGHT BAR === -->

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?php include_once "blog-rightbat.php"; ?>
            </div>


        </div>
    </div>
</div>

