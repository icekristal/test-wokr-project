
<!-- ===================================
    PAGE HEADER
======================================== -->
<div class="page-header" data-stellar-background-ratio="0.4">
    <div class="page-header-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- === PAGE HEADER TITLE === -->
            <div class="page-header-title">
                <h2>БЛОГ</h2>
            </div>

            <!-- === PAGE HEADER BREADCRUMB === -->
            <div class="page-header-breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Блог</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- ===================================
    END PAGE HEADER
======================================== -->

<!-- =========================
    BLOG ITEMS
============================== -->
<div class="def-section blog-section">
    <div class="container">
        <div class="row">

            <!-- === BLOG ITEMS === -->

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 blog-items">

<?php if (isset($all_article[0])){ ?>
<?php foreach ($all_article as $article){ ?>
                <!-- === BLOG ITEM === -->
                <div class="blog-item">
                    <div class="blog-item-date">
                        <?= $article['date_blog'] ?>
                    </div>
                    <div class="blog-item-image">
                        <img src="/image/download/blog_main/<?=$article['file']?>" alt="<?= $article['title'] ?>" />
                    </div>
                    <div class="blog-item-info">
                        <span class="author-icon"><i class="fa fa-user"></i></span>
                        <a href="#">Admin</a>
                        <span class="date-icon"><i class="fa fa-calendar"></i></span>
                        <a href="<?=$article['href_article']?>">26 may 2015</a>
                    </div>
                    <div class="blog-item-title">
                        <h3><a href="<?=$article['href_article']?>"><?=$article['title']?></a></h3>
                    </div>
                    <div class="blog-item-text">
                        <p>
                            <?=$article['description']?> </p>
                    </div>
                    <div class="blog-item-button">
                        <a href="<?= $article['href_article']?>"><div class="my-btn">
                                <div class="my-btn-bg-top"></div>
                                <div class="my-btn-bg-bottom"></div>
                                <div class="my-btn-text">
                                    Читать дальше
                                </div>
                            </div></a>
                    </div>
                </div>
          <?php } ?>
                <?php }else{ ?>
    <h3>Пока нет статей=(</h3>
                <?php } ?>
            </div>

            <!-- === BLOG RIGHT BAR === -->

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?php include_once "blog-rightbat.php"; ?>
            </div>


        </div>
    </div>
</div>



	
	