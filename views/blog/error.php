

<!-- ===================================
    PAGE HEADER
======================================== -->
<div class="page-header" data-stellar-background-ratio="0.4">
    <div class="page-header-overlay"></div>
    <div class="container">
        <div class="row">

            <!-- === PAGE HEADER TITLE === -->
            <div class="page-header-title">
                <h2>Ошибка 404 - страница не найдена</h2>
            </div>

            <!-- === PAGE HEADER BREADCRUMB === -->
            <div class="page-header-breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Ошибка</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- ===================================
    END PAGE HEADER
======================================== -->

<!-- =========================
    ABOUT TEXT
============================== -->
<div class="def-section about-text">
    <div class="container">
        <div class="row">

            <div class="home-blog-more">
                <a href="/blog"><div class="my-btn my-btn-primary">
                        <div class="my-btn-bg-top"></div>
                        <div class="my-btn-bg-bottom"></div>
                        <div class="my-btn-text">
                            Читать блог
                        </div>
                    </div></a>
            </div>

        </div>
    </div>
</div>
<!-- =========================
    END ABOUT TEXT
============================== -->

