

function LoginTopUser() {
    var msg   = $('#logintopform').serialize();
    $('#MessageAuth').html("");
    $.ajax({
        url: '/functiontop/authusertop/',
        async : false,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: msg,
        beforeSend: function(data) {
           $('#MessageAuth').html('Проверяем данные...');
        },
        success: function(data) {
            if(data['success']){
                $('#MessageAuth').html(data['message']);
                location.href='/admin/';

            }else{
                $('#MessageAuth').html(data['message']);
            }
        },
        error : function () {
            alert("ДА");
        }
    });
}

function registerNewUserTop() {
    var msg   = $('#registrationtopform').serialize();

    $.ajax({
        url: '/functiontop/registerusertop/',
        async : false,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: msg,
        beforeSend: function(data) {
            $('#MessageRegistration').html('Идет регистрация...');
        },
        success: function(data) {
            if(data['success']){
                $('#MessageRegistration').html(data['message']);
                location.href='/admin/auth';
            }else{
                $('#MessageRegistration').html(data['message']);
            }
        }
    });


}



function addnewcategory() {
    var gvar = new FormData($('#addnewcategory')[0]);

    $('#publication_add_button').attr('disabled',true);
    $.ajax({
        url: '/functiontop/addnewcatpublic/',
        processData: false,
        contentType: false,
        cache:false,
        dataType: "json", // тип передачи данных
        type: "post", // метод передачи
        data: gvar,
        success: function(data) {

            $('#publication_add_button').attr('disabled',false);
            if(data['success']){
                $('#placemessage').html(data['message']);
               $('#addnewcategory').trigger( 'reset' );
               $('.refseshtoplist').load('/admin/blog/ .refseshbottomlist');

            }else{
                $('#placemessage').html(data['message']);
            }
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + str + xhr);
        }
    });
}

function SeeCategoryInModal(id) {
    $.ajax({
        url: '/functiontop/seeallcategoryinmodal/',
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: {
            "id":id
        },
        success: function(data) {
            if(data['success']){
                $('#seecategory').html(data['message']);
            }else{
                $('#seecategory').html(data['message']);
            }
        }
    });

}

/*Публикации*/
function CreateBlog() {
    var gvar;
    gvar = new FormData($('#createblog')[0]);
    dataqq=CKEDITOR.instances.textar.getData();
    $('#button_create_blog').attr('disabled',true);
    $.ajax({
        url: '/functiontop/addnewpublication/',
        processData: false,
        contentType: false,
        cache:false,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: gvar,
        success: function(data) {
            $('#button_create_blog').attr('disabled',false);
            if(data['success']){
                $('.refseshtoplist').load('/admin/blog/ .refseshbottomlist');
                $('#messageblog').html(data['message']);
                $('#createblog').trigger( 'reset' );

            }else{
                $('#messageblog').html(data['message']);
            }
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + str + xhr);
        }
    });

}


function SeePublicInModal(id) {
    $.ajax({
        url: "/functiontop/viewonepublic/", // куда отправляем
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: { // что отправляем
            "id":id
        },
        beforeSend : function () {
            $('#oknoeditblog').html();
        },
        success: function(data){
            if(data['success']){
                $('#oknoeditblog').html(data['message']);

            }
        }



    });
}

function EditBlog() {
    var gvar = new FormData($('#editblog')[0]);
    $('#butsaveblog').attr('disabled',false);
    dataqq=CKEDITOR.instances.textarq.getData();
    $.ajax({
        url: '/functiontop/editpublication/',
        processData: false,
        contentType: false,
        cache:false,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: gvar,
        success: function(data) {
            $('#butsaveblog').attr('disabled',false);
            if(data['success']){
                $('#message_edit_blog').html(data['message']);
                $('.refseshtoplist').load('/admin/blog/ .refseshbottomlist');


            }else{
                $('#message_edit_blog').html(data['message']);
            }
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + str + xhr);
        }
    });
}

function DeletePublic(id,vis) {

    $.ajax({
        url: '/functiontop/deletepublication/',
        async : true,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: {
            "id" : id,
            "visible" : vis
        },
        success: function(data) {
            if(data['success']){
                $('.refseshtoplist').load('/admin/blog/ .refseshbottomlist');
            }else{

            }
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + str + xhr);
        }
    });
}

function DeleteCategoryPublic(id,vis) {

    $.ajax({
        url: '/functiontop/deletecategorypublication/',
        async : true,
        type: "post", // метод передачи
        dataType: "json", // тип передачи данных
        data: {
            "id" : id,
            "visible" : vis
        },
        success: function(data) {
            if(data['success']){
                $("#catblog_"+id).hide();
                $('#placemessage').html(data['message']);
            }else{
                $('#placemessage').html(data['message']);
            }
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + str + xhr);
        }
    });
}


function previewFile(idfile,idview) {

    var preview = document.getElementById(idview);
    var file    = document.querySelector('#'+idfile+' input[type=file]').files[0];
    var reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);

    }
}

function previewFileHref(href,idview) {
    var preview = document.getElementById(idview);
    preview.src = href;

}



function CKupdate() {
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
}





$(function() {
    $("textarea[id='description']").keyup(function countRemainingChars(){
        maxchars = 150;
        number = $("textarea[id='description']").val().length;
        if(number <= maxchars){
            $("#textsimv").html(maxchars-number + " символов осталось");
        }

    });


    $("textarea[id='textar']").keyup(function countRemainingChars(){
        maxcharsz = 750;
        number = $("textarea[id='textar']").val().length;
        if(number <= maxcharsz){
            $("#textsimvosn").html(maxcharsz-number + " символов осталось");
        }
    });
});
