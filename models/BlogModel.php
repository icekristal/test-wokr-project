<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 29.07.2017
 * Time: 10:01
 */




use fnc\DateFormat;
use fnc\Translate;
use fnc\Validator;


class BlogModel
{
    const SHOW_DEFAULT_ARTICLE = 6;

    public static function getAllArticle($count=self::SHOW_DEFAULT_ARTICLE,$page=1,$limit=null){

        $count = intval($count);
        $page = intval($page);
        $offset = ($page-1)*$count;

        $db = DataBase::getConnection();
        $all_article = array();

        $rs = $db->query("SELECT catblog.name,catblog.keyc,mainar.* FROM `blog_article` as `mainar` 
        JOIN `blog_category` as `catblog` ON category=catblog.id
        WHERE mainar.`time_public` < NOW() and `mainar`.`visible`=1 ORDER BY mainar.`time_public` DESC LIMIT {$count} OFFSET {$offset}");

        if(isset($limit)){
            $rs = $db->query("SELECT catblog.name,catblog.keyc,mainar.* FROM `blog_article` as `mainar` 
JOIN `blog_category` as `catblog` ON category=catblog.id
 WHERE mainar.`time_public` < NOW()  and `mainar`.`visible`=1 ORDER BY RAND() LIMIT {$limit}");
        }

       // $rs->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $rs->fetch())
        {
            $row['href_article']="/blog/{$row['keyc']}/{$row['title_url']}/";
            /*Красивая дата*/
            $end_data = '';
            $dat_ra = date_create($row['time_public']);
            $data_day=date_format($dat_ra, 'd');
            $data_year=date_format($dat_ra, 'Y');
            $data_mouth=date_format($dat_ra, 'n');
            $data_mouth =  DateFormat::RusMountsPadezh($data_mouth);
            $data_mouth = mb_substr($data_mouth,0,3);
            $end_data = "  <div class=\"home-blog-item-date-number\">{$data_day}</div>{$data_mouth}";
            $row['date_blog']=$end_data;

            $row['date_blog_rightbar']="{$data_day} {$data_mouth} {$data_year}";
            /* -- Красивая дата*/

            $all_article[]=$row;
        }

        return $all_article;

    }




    public static function getCountAllArticle($category_id=null){
        $db = DataBase::getConnection();

        $rs = $db->query("SELECT count(`id`) as count FROM `blog_article` WHERE `visible`=1");

            if(isset($category_id)){
                $rs = $db->query("SELECT count(`id`) as count FROM `blog_article` WHERE `category`='{$category_id}' AND `visible`=1");

            }
        //$rs->setFetchMode(PDO::FETCH_ASSOC);
        $row = $rs->fetch();
        return $row['count'];
    }

    public static function getAllArticleForCategory($count=self::SHOW_DEFAULT_ARTICLE,$page=1,$category_id,$limit=null){

        $count = intval($count);
        $page = intval($page);
        $offset = ($page-1)*$count;

        $db = DataBase::getConnection();
        $all_article = array();

        $rs = $db->query("SELECT catblog.name,catblog.keyc,mainar.* FROM `blog_article` as `mainar` 
        JOIN `blog_category` as `catblog` ON category=catblog.id
        WHERE mainar.`time_public` < NOW() and mainar.category='{$category_id}' and `mainar`.`visible`=1 ORDER BY mainar.`time_public` DESC LIMIT {$count} OFFSET {$offset}");

        if(isset($limit)){
            $rs = $db->query("SELECT * FROM `blog_article` WHERE `time_public` < NOW() ORDER BY `time_public` DESC LIMIT {$limit}");
        }

        // $rs->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $rs->fetch())
        {
            $row['href_article']="/blog/{$row['keyc']}/{$row['title_url']}/";
            /*Красивая дата*/
            $end_data = '';
            $dat_ra = date_create($row['time_public']);
            $data_day=date_format($dat_ra, 'd');
            $data_year=date_format($dat_ra, 'Y');
            $data_mouth=date_format($dat_ra, 'n');
            $data_mouth =  DateFormat::RusMountsPadezh($data_mouth);
            $data_mouth = mb_substr($data_mouth,0,3);
            $end_data = "  <div class=\"home-blog-item-date-number\">{$data_day}</div>{$data_mouth}";
            $row['date_blog']=$end_data;

            $row['date_blog_rightbar']="{$data_day} {$data_mouth} {$data_year}";
            /* -- Красивая дата*/

            $all_article[]=$row;
        }

        return $all_article;

    }


    public static function getArticleForId($id_article){
        $db = DataBase::getConnection();
        $rs = $db->query("SELECT `bcat`.name,`bcat`.keyc,`fart`.* FROM `blog_article` as `fart`
JOIN blog_category as `bcat` ON category=bcat.id
WHERE `fart`.id='{$id_article}' and fart.`visible`=1 LIMIT 1");
        $all_article =array();
        while ($row = $rs->fetch())
        {
            $row['href_article']="/blog/{$row['keyc']}/{$row['title_url']}/";
            /*Красивая дата*/
            $end_data = '';
            $dat_ra = date_create($row['time_public']);
            $data_day=date_format($dat_ra, 'd');
            $data_year=date_format($dat_ra, 'Y');
            $data_mouth=date_format($dat_ra, 'n');
            $data_mouth =  DateFormat::RusMountsPadezh($data_mouth);
            $data_mouth = mb_substr($data_mouth,0,3);
            $end_data = "  <div class=\"home-blog-item-date-number\">{$data_day}</div>{$data_mouth}";
            $row['date_blog']=$end_data;

            $row['date_blog_rightbar']="{$data_day} {$data_mouth} {$data_year}";
            /* -- Красивая дата*/

            $all_article[]=$row;
        }
        return $all_article[0];
    }

//Проверяем на существование категории, если да,возвращаем  ее id
    public static function checkExistCategory($category_key){
        $db = DataBase::getConnection();
        $rs = $db->query("SELECT * FROM `blog_category` WHERE keyc='{$category_key}'");
        $row = $rs->fetch();
            if($row['id']>0){
                return $row['id'];
            }else{
                return false;
            }
    }

    //Проверяем на существование статьи, если да,возвращаем  ее id
    public static function checkExistrticle($title_url){
        $db = DataBase::getConnection();
        $rs = $db->query("SELECT * FROM `blog_article` WHERE title_url='{$title_url}' and `time_public`<NOW() and `visible`=1");
        $row = $rs->fetch();
        if($row['id']>0){
            return $row['id'];
        }else{
            return false;
        }
    }

    //Получаем все категории
    public static function getAllCategoryBlog($limit_and_random = null){
        $db = DataBase::getConnection();
        $sql = "SELECT * FROM `blog_category` WHERE `visible`=1";
        if(isset($limit_and_random)){
            $sql.=" ORDER BY RAND() LIMIT {$limit_and_random}";
        }
        $rs = $db->query($sql);
        return $rs->fetchAll();
    }

    //Получаем информацию о категории
    public static function getOneCategoryBlog($id_category){
        $db = DataBase::getConnection();
        $rs = $db->query("SELECT * FROM `blog_category` WHERE `id`='{$id_category}'");
        return $rs->fetch();
    }

//Для админа

    public static function getAllArticleAdmin(){



        $db = DataBase::getConnection();
        $all_article = array();

        $rs = $db->query("SELECT catblog.name,catblog.keyc,mainar.* FROM `blog_article` as `mainar` 
        JOIN `blog_category` as `catblog` ON category=catblog.id WHERE mainar.visible=1
       ORDER BY mainar.`time_public` DESC");


        // $rs->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $rs->fetch())
        {
            $all_article[]=$row;
        }

        return $all_article;

    }

    public static function makeNewCategoryForBlog($category_name){

        $category_key =  Translate::str2url($category_name);
        $db = DataBase::getConnection();
       $rs = $db->query("INSERT INTO blog_category (`name`,`keyc`) VALUE ('{$category_name}','{$category_key}')");
        return $rs;
    }

    public static function AddPublication($category_id, $title,$title_url, $descpription, $text, $date_publication,$image,$who_create = "admin"){
        $db = DataBase::getConnection();
        $rs = $db->prepare("INSERT INTO `blog_article`
 (`title`,`title_url`,`description`,`text_article`,`category`,`file`,`time_public`,`admin_dob`,`data_dob`)
VALUES (:title,:titleurl,:descpription,'{$text}',:categoryid,:image,'{$date_publication}','{$who_create}',NOW())");
        $rs->bindParam(":title",$title,PDO::PARAM_STR);
        $rs->bindParam(":titleurl",$title_url,PDO::PARAM_STR);
        $rs->bindParam(":descpription",$descpription,PDO::PARAM_STR);
        $rs->bindParam(":image",$image,PDO::PARAM_STR);
        $rs->bindParam(":categoryid",$category_id,PDO::PARAM_INT);
        return $rs->execute();

    }

    public static function UpdatePublication($id,$category_id,$title,$title_url,$descriprion,$text,$datatimepublic,$name_image){
        $db = DataBase::getConnection();
        $rs  = $db->prepare("UPDATE `blog_article` SET 
          `category`=:category,
          `title`=:title,
          `title_url`=:titleurl,
          `description`=:description,
          `text_article`='{$text}',
          `time_public`='{$datatimepublic}',
          `file`='{$name_image}'
          WHERE `id`=:id
          ");

        $rs->bindParam(":title",$title,PDO::PARAM_STR);
        $rs->bindParam(":titleurl",$title_url,PDO::PARAM_STR);
        $rs->bindParam(":description",$descriprion,PDO::PARAM_STR);
        $rs->bindParam(":category",$category_id,PDO::PARAM_INT);
        $rs->bindParam(":id",$id,PDO::PARAM_INT);
        return $rs->execute();
    }

    public static function DeletePublication($id,$action){
        $id = intval($id);
        $db = DataBase::getConnection();
        $rs = $db->prepare("UPDATE `blog_article` SET `visible`=:actionq WHERE id=:id");
        $rs->bindParam(":actionq",$action,PDO::PARAM_INT);
        $rs->bindParam(":id",$id,PDO::PARAM_INT);
        return $rs->execute();
    }

    public static function setCountReadArticle($id_article){
        $id_article = intval($id_article);
        $db = DataBase::getConnection();
        $rs = $db->query("UPDATE `blog_article` SET `count_read`=`count_read`+1 WHERE `id`='{$id_article}'");
        return $rs;

    }

    public static function DeleteCategoryPublication($id,$action){
        $id = intval($id);
        $db = DataBase::getConnection();
        $rs = $db->prepare("UPDATE `blog_category` SET `visible`=:actionq WHERE id=:id");
        $rs->bindParam(":actionq",$action,PDO::PARAM_INT);
        $rs->bindParam(":id",$id,PDO::PARAM_INT);
        return $rs->execute();
    }

}