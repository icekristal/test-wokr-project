<?php
/**
 * Created by PhpStorm.
 * User: Savatneev Anton
 * Date: 08.08.2017
 * Time: 10:56
 */




use fnc\Validator;


class UsersTopModel
{

    public static function checkRightUser($need_right){
        $user = self::CheckLoginUser();
        $user_right = $user['right'];
        if($need_right>$user_right){
            redirect("/noright/");
            return false;
        }
        return true;
    }

    public static function checkLoginAdmin($login,$password){
        $db = DataBase::getConnection();
        $password = GenPassAdmin($password,$login);
        $rs = $db->prepare("SELECT * FROM `user_admin`
          WHERE `login`=:login and `password`=:password and `right`>0 ORDER BY `id` LIMIT 1
          ");
        $rs->bindParam(":login",$login,PDO::PARAM_STR);
        $rs->bindParam(":password",$password,PDO::PARAM_STR);
        $rs->execute();
        $row = $rs->fetch();
            if(isset($row["id"])){
                $row['success']=1;
            }else{
                $row['success']=0;
            }
            return $row;
    }

    public static function AuthSessionAdmin($id){
        $db = DataBase::getConnection();
        $id = intval($id);
        $rs = $db->prepare("SELECT * FROM `user_admin`
        WHERE `id`=:id ORDER BY `id` LIMIT 1
");
        $rs->bindParam(":id",$id,PDO::PARAM_INT);
        $rs->execute();
        $row = $rs->fetch();
        if(isset($row["id"])){

            $row['success']=1;
           $_SESSION['usertop']=$row;
        }else{
            $row['success']=0;
            unset($_SESSION['usertop']);
        }
        return $row;
    }

    public static function CheckLoginUser(){
            if(isset($_SESSION['usertop'])){
                return $_SESSION['usertop'];
            }
        redirect("/admin/auth/");
            return false;
    }


    public static function checkRegistrationParams($login,$password,$passwordq,$name,$sname,$email){
        $rezData = array();

        $rezData['success']=1;
        $rezData['message']="Все данные успешно прошли проверку";
            if(strlen($login)<=6){
                $rezData['success']=0;
                $rezData['message']="Минимальная длина логина 6 символов";
            }


        if(strlen($password)<=8){
            $rezData['success']=0;
            $rezData['message']="Минимальная длина пароля 8 символов";
        }

        if(!Validator::CheckingEmail($email)){
            $rezData['success']=0;
            $rezData['message']="Не верно введен Email";
        }

        if(strlen($name)<=2 or strlen($sname)<=2){
            $rezData['success']=0;
            $rezData['message']="Минимальная длина имени и фамилии 2 символа";
        }

        if($password!=$passwordq){
            $rezData['success']=0;
            $rezData['message']="Пароли не совпадают";
        }

        return $rezData;
    }


    public static function checkExistParam($login,$email){
        $rezData = array();
        $rezData['success']=1;
        $rezData['message']="Проверка на допустимость значений пройдена";
            $db = DataBase::getConnection();
            $rs = $db->prepare("SELECT * FROM `user_admin`
            WHERE `login`=:login LIMIT 1
            ");
            $rs->bindParam(":login",$login,PDO::PARAM_STR);
            $rs->execute();
            $row = $rs->fetch();
            if(isset($row['id'])){
                $rezData['success']=0;
                $rezData['message']="Такой логин уже есть";
            }

            if(isset($row['email'])){
                if($row['email']==$email){
                    $rezData['success']=0;
                    $rezData['message']="Такой Email уже есть";
                }
            }


        return $rezData;
    }

    public static function RegistationUserTop($login,$password,$name,$sname,$email){

        $db = DataBase::getConnection();
        $rs = $db->prepare("INSERT INTO `user_admin`
        (`name`,`s_name`,`login`,`password`,`email`,`data_registr`)
        VALUES (:name,:s_name,:login,:password,:email,NOW())
");
        $rs->bindParam(":name",$name,PDO::PARAM_STR);
        $rs->bindParam(":s_name",$sname,PDO::PARAM_STR);
        $rs->bindParam(":login",$login,PDO::PARAM_STR);
        $rs->bindParam(":password",$password,PDO::PARAM_STR);
        $rs->bindParam(":email",$email,PDO::PARAM_STR);
        $rs->execute();
        return $rs;
        }


    public static function getCountAllTopUsers(){
        $db = DataBase::getConnection();

        $rs = $db->query("SELECT count(`id`) as count FROM `user_admin` WHERE `right`>0");

        $row = $rs->fetch();
        return $row['count'];
    }
}