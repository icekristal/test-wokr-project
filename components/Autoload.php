<?php

function __autoload($class_name)
{
    // Массив папок, в которых могут находиться необходимые классы
    $array_paths = array(
        '/models/',
        '/components/',
        '/controllers/',
    );
    // Проходим по массиву папок
    foreach ($array_paths as $path) {
        // Формируем имя и путь к файлу с классом

        $pathz = S_ROOT . $path . $class_name . '.php';

        // Если такой файл существует, подключаем его
        if (is_file($pathz)) {
            include_once $pathz;
        }
    }
}