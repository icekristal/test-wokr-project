<?php




use fnc\Entrance;
use fnc\Logger;
use fnc\MessageVoice;

class SetSmart
{

    public $file_name = "";
    private $last_prefix = TemplatePostfix;
    private $entrance = "";
    public function __construct(){


        $this->entrance =  new Entrance("usertop");



    }

    /**
     * @param string $file_name
     */
    public function LoadSmarty($file_name)
    {
      require_once(S_ROOT . "/views/blog/".$file_name.$this->last_prefix);
    }

    protected function UserInfoAuth(){
        return $this->entrance;
    }

    protected function MessageSend($message_status,$message_text){
        $mmt = new MessageVoice();
        return $mmt->MessageSend($message_status,$message_text);
    }

    protected function Logger($type_message,$name_user,$message_text){
        return  new Logger($name_user,$message_text,$type_message);
    }
}