<?php



class Router{

    private $routes;

    function __construct()
    {

        $routesPath = S_ROOT."/config/routes.php";
        $this->routes = include($routesPath);
    }

    private function getURL(){
            if(!empty($_SERVER['REQUEST_URI'])){
                return trim($_SERVER['REQUEST_URI'],"/");
            }else{
                return false;
            }
    }

    /**
     *
     */
    public function run()
    {
        $url = $this->getURL();
        $check_user_error = false;

        foreach ($this->routes as $urlPattern => $path){

            if(preg_match("~{$urlPattern}~",$url)){

                $internalRoute = preg_replace("~{$urlPattern}~",$path,$url);


                $segment = explode("/",$internalRoute);



                $controllerName = array_shift($segment).PathPostfixController;
                $controllerName = ucfirst($controllerName);
                $actionName = ucfirst(array_shift($segment)).PathPostfixAction;

                $parameters = $segment;

                $ControllerFile = S_ROOT.PathPrefixController.$controllerName.".php";


                $result = null;
                if(file_exists($ControllerFile)){
                    include_once ($ControllerFile);
                    $ControllerObject = new $controllerName;

                    if(method_exists($ControllerObject,$actionName)){
                        $check_user_error = true;
                        $result = call_user_func_array(array($ControllerObject,$actionName),$parameters);
                    }
                }


                if($result!=null){
                    break;
                }
            }

        }

            if($check_user_error==false){
                include_once (S_ROOT.PathPrefixController."ErrorController.php");
                $ControllerObject = new ErrorController();
                $ControllerObject->IndexAction();
            }

    }


}