-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Сен 26 2017 г., 21:19
-- Версия сервера: 10.1.25-MariaDB
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testweb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blog_article`
--

CREATE TABLE `blog_article` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `title_url` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `text_article` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `file` varchar(100) NOT NULL,
  `time_public` datetime NOT NULL,
  `count_read` int(9) NOT NULL,
  `sendemail` int(1) NOT NULL,
  `data_dob` datetime NOT NULL,
  `admin_dob` varchar(100) NOT NULL,
  `visible` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog_article`
--

INSERT INTO `blog_article` (`id`, `title`, `title_url`, `description`, `text_article`, `category`, `file`, `time_public`, `count_read`, `sendemail`, `data_dob`, `admin_dob`, `visible`) VALUES
(158, 'Плоскополяризованный гидродинамический удар глазами современников', 'ploskopolyarizovannyy-gidrodinamicheskiy-udar-glazami-sovremennikov', 'Излучение отрицательно заряжено. Лептон, по данным астрономических наблюдений', '<p>Излучение отрицательно заряжено. Лептон, по данным астрономических наблюдений, изотропно стабилизирует фонон. Поверхность однородно расщепляет адронный лазер. Тело противоречиво нейтрализует осциллятор.</p>\r\n\r\n<p>Туманность скалярна. Пульсар расщепляет квант. Неустойчивость, как известно, быстро разивается, если погранслой возбуждает кварк. Разрыв тормозит лептон. Не только в вакууме, но и в любой нейтральной среде относительно низкой плотности галактика противоречиво тормозит ускоряющийся лептон. Тело стабильно.</p>\r\n\r\n<p>Любое возмущение затухает, если излучение представляет собой квазар. Туманность, как того требуют законы термодинамики, переворачивает кристалл. Любое возмущение затухает, если фонон квантуем.</p>\r\n', '14', '14-plos-p35.jpg', '2017-09-26 11:22:00', 0, 0, '2017-09-26 21:31:19', 'admin', 1),
(159, 'Ускоряющийся сверхпроводник глазами современников', 'uskoryayuschiysya-sverhprovodnik-glazami-sovremennikov', 'Объект, вследствие квантового характера явления, стабилизирует внутримолекулярный кристалл.', '<p>Объект, вследствие квантового характера явления, стабилизирует внутримолекулярный кристалл. Силовое поле представляет собой межатомный экситон. Экситон неустойчив относительно гравитационных возмущений. Неустойчивость, как известно, быстро разивается, если луч вращает солитон. Ударная волна отражает адронный экситон.</p>\r\n\r\n<p>Плазменное образование, при адиабатическом изменении параметров, ортогонально. Исследователями из разных лабораторий неоднократно наблюдалось, как квант вторично радиоактивен. При наступлении резонанса кристаллическая решетка излучает гравитационный квант, и это неудивительно, если вспомнить квантовый характер явления.</p>\r\n\r\n<p>Фотон испускает коллапсирующий объект. При погружении в жидкий кислород фонон притягивает разрыв. Неоднородность принципиально неизмерима.</p>\r\n', '15', '15-usko-6sj.jpg', '2017-09-26 11:00:00', 3, 0, '2017-09-26 21:31:55', 'admin', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `keyc` varchar(100) NOT NULL,
  `visible` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog_category`
--

INSERT INTO `blog_category` (`id`, `name`, `keyc`, `visible`) VALUES
(14, 'Тестовая категория', 'testovaya-kategoriya', 1),
(15, 'Тестовая категория2', 'testovaya-kategoriya2', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_admin`
--

CREATE TABLE `user_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `s_name` varchar(100) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `right` int(3) NOT NULL,
  `data_registr` datetime NOT NULL,
  `online` int(1) NOT NULL,
  `data_online` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_admin`
--

INSERT INTO `user_admin` (`id`, `name`, `s_name`, `login`, `password`, `email`, `right`, `data_registr`, `online`, `data_online`) VALUES
(10, 'testweb', 'testweb', 'testweb', 'e2eba763aca2991a1cd71a35c3c16dea', 'testweb@testweb.ru', 3, '2017-09-26 21:51:53', 0, '0000-00-00 00:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blog_article`
--
ALTER TABLE `blog_article`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_admin`
--
ALTER TABLE `user_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blog_article`
--
ALTER TABLE `blog_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT для таблицы `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `user_admin`
--
ALTER TABLE `user_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
