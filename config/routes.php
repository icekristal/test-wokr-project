<?php
return array(
    //Блог
    'blog/page-([0-9]+)'=>'blog/index/$1',
    'blog/([a-z0-9.;-]+)/page-([0-9]+)'=>'blog/category/$1/$2',
    'blog/([a-z0-9.;-]+)/([a-z.;-]+)'=>'blog/article/$1/$2',
    'blog/([a-z0-9.;-]+)'=>'blog/category/$1',
    'blog'=>"blog/index",
    //Админ панель

        //Вход
    "admin/auth"=>'adminAuth/auth',
    "admin/registration"=>'adminAuth/registration',

    "admin/blog"=>'admin/blog',
    "noright"=>'admin/noright',
    "admin"=>'admin/index',

    //Фукнций администратора
    "functiontop/addnewcatpublic"=>"admin/addnewcatpublic",
    "functiontop/deletecategorypublication"=>"admin/deletecategorypublication",
    "functiontop/addnewpublication"=>"admin/addnewpublication",
    "functiontop/viewonepublic"=>"admin/viewonepublic",
    "functiontop/editpublication"=>"admin/editpublication",
    'functiontop/deletepublication'=>"admin/deletepublication",
	
    'functiontop/logout'=>"admin/logout",



    'functiontop/authusertop'=>"adminAuth/authusertop",
    'functiontop/registerusertop'=>"adminAuth/registrationUserTop",

    //Страницы сайта
    'index'=>"index/index",

    //Ошибочная
    'error'=>"error/index",
    ''=>"index/index"
);